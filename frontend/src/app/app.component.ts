import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

    secondes: number;
    counterSubscription: Subscription;

    lastUpdate = new Promise((resolve, reject) => {
        const date = new Date();
        setTimeout(
            () => {
                resolve(date);
            }, 2000
        );
    });

    ngOnInit(): void {
        const counter = interval(1000);
        this.counterSubscription = counter.subscribe(
            (value) => {
                this.secondes = value;
            },
            (error) => {
                console.error('Uh-oh, an error occurred! : ' + error);
            },
            () => {
                console.info('Observable complete!');
            }
        );
    }

    ngOnDestroy(): void {
        this.counterSubscription.unsubscribe();
    }


}
