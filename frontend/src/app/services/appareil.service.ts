import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Appareil } from '../models/Appareil.model';

@Injectable()
export class AppareilService {
    private appareils: Array<Appareil> = [];

    appareilsSubject = new Subject<any[]>();

    constructor(private httpClient: HttpClient, private router: Router) {
        this.getAppareilsFromServer();
    }

    emitAppareilSubject() {
        // multicast to Observers
        this.appareilsSubject.next(this.appareils.slice());
    }

    getAppareilById(id: number) {
        const appareil = this.appareils.find(
            (s) => {
                return s.id === id;
            }
        );
        return appareil;
    }

    switchOnAll() {
        for (let appareil of this.appareils) {
            appareil.status = 'allumé';
        }
        this.emitAppareilSubject();
    }

    switchOffAll() {
        for (let appareil of this.appareils) {
            appareil.status = 'éteint';
        }
        this.emitAppareilSubject();
    }

    switchOnOne(i: number) {
        this.appareils[i].status = 'allumé';
        this.emitAppareilSubject();
    }

    switchOffOne(i: number) {
        this.appareils[i].status = 'éteint';
        this.emitAppareilSubject();
    }

    addAppareil(name: string, status: string) {
        const appareilObject = new Appareil(0, '', '');
        appareilObject.name = name;
        appareilObject.status = status;
        //appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
        this.httpClient
            .post('http://localhost:8080/appareil', appareilObject)
            .subscribe(
                () => {
                    console.log('Enregistrement terminé !');
                },
                (error) => {
                    console.log('Erreur ! : ' + error);
                }
            );
        //this.appareils.push(appareilObject);
        //this.emitAppareilSubject();
        this.router.navigate(['appareils']);
    }

    getAppareilsFromServer() {
        this.httpClient
            .get<any[]>('http://localhost:8080/appareils')
            .subscribe(
                (response) => {
                    this.appareils = response;
                    this.emitAppareilSubject();
                },
                (error) => {
                    console.log('Erreur ! : ' + error);
                }
            );
    }
}