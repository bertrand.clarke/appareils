import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppareilService } from '../services/appareil.service';

@Component({
    selector: 'app-edit-appareil',
    templateUrl: './edit-appareil.component.html',
    styleUrls: ['./edit-appareil.component.scss']
})
export class EditAppareilComponent implements OnInit {

    defaultOnOff = 'éteint';

    constructor(private appareilService: AppareilService, private router: Router) { }

    ngOnInit() {
    }

    onSubmit(form: NgForm) {
        console.info("Ajout d'un appareil : " + form.value);
        this.appareilService.addAppareil(form.value['name'], form.value['status']);
        this.router.navigate(['/appareils'])
    }
}
