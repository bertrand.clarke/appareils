import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppareilService } from '../services/appareil.service'


@Component({
    selector: 'app-appareil-view',
    templateUrl: './appareil-view.component.html',
    styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit, OnDestroy {

    isAuth = false;
    appareils: any;
    appareilSubscription: Subscription;

    constructor(private appareilService: AppareilService) { }

    ngOnInit() {
        this.appareilSubscription = this.appareilService.appareilsSubject.subscribe(
            (appareils: any[]) => {
                this.appareils = appareils;
            }
        );
        this.appareilService.emitAppareilSubject();
        this.appareilService.getAppareilsFromServer();
    }

    ngOnDestroy(): void {
        this.appareilSubscription.unsubscribe();
    }

    onAllumer() {
        this.appareilService.switchOnAll();
    }

    onEteindre() {
        if (confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
            this.appareilService.switchOffAll();
        } else {
            return null;
        }
    }

    onFetch() {
        this.appareilService.getAppareilsFromServer();
    }

}
