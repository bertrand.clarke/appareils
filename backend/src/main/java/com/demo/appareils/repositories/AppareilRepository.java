package com.demo.appareils.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.appareils.entities.Appareil;

@Repository
public interface AppareilRepository extends JpaRepository<Appareil, Long> {
	List<Appareil> findByNameLike(String pattern);
}