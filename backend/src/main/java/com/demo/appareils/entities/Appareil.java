package com.demo.appareils.entities;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;

@Entity
public class Appareil {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Length(min = 3, max = 20)
	private final String name;

	@Enumerated(EnumType.STRING)
	@Convert(converter = StatusConverter.class)
	private final Status status;

	private String attributNonExpose;

	public Appareil() {
		this.name = "";
		this.status = null;
	}

	public Appareil(String name, Status status) {
		this.name = name;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Status getStatus() {
		return status;
	}

	public String getAttributNonExpose() {
		return attributNonExpose;
	}

	public void setAttributNonExpose(String attributNonExpose) {
		this.attributNonExpose = attributNonExpose;
	}

	@Override
	public String toString() {
		return "Appareil{" + "id=" + id + ", name=" + name + ", status=" + status + ", attributNonExpose="
				+ attributNonExpose + "}";
	}

}