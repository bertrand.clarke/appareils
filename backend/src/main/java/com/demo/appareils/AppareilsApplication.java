package com.demo.appareils;

import java.util.logging.Logger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.demo.appareils.repositories.AppareilRepository;
import com.demo.appareils.repositories.UserRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2 // Accessible sur /swagger-ui.html
public class AppareilsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppareilsApplication.class, args);
	}

	@Bean
	CommandLineRunner init(UserRepository userRepository, AppareilRepository appareilRepository) {
		return args -> {
			Logger.getLogger(AppareilsApplication.class.getName()).info("Users : " + userRepository.findAll());
			Logger.getLogger(AppareilsApplication.class.getName()).info("Appareils : " + appareilRepository.findAll());
		};
	}

	// On utilise plutot data.sql pour populer la base
//	@Bean
//	CommandLineRunner init(UserRepository userRepository, AppareilRepository appareilRepository) {
//		return args -> {
//			Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
//				User user = new User(name, name.toLowerCase() + "@domain.com");
//				userRepository.save(user);
//			});
//			userRepository.findAll().forEach(System.out::println);
//			appareilRepository.save(new Appareil("Frigo", Status.allumé));
//			appareilRepository.save(new Appareil("Four", Status.éteint));
//		};
//	}
}
