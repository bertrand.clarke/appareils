package com.demo.appareils.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.demo.appareils.entities.Appareil;
import com.demo.appareils.repositories.AppareilRepository;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@Api(tags = { "API appareils" })
@SwaggerDefinition(tags = { @Tag(name = "API appareils", description = "API pour les appareils") })
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AppareilController {

	private final AppareilRepository appareilRepository;

	public AppareilController(AppareilRepository appareilRepository) {
		this.appareilRepository = appareilRepository;
	}

	@ApiOperation(value = "Récupère les appareils")
	@GetMapping("/appareils")
	public List<Appareil> getAppareils() {

		// On peut masquer certaines propriétés pour cette requête pour une api public
		// par exemple
		SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("attributNonExpose");
		FilterProvider filtres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);
		MappingJacksonValue appareilsFiltres = new MappingJacksonValue(appareilRepository.findAll());
		appareilsFiltres.setFilters(filtres);
		return appareilRepository.findAll();
	}

	@PostMapping("/appareil")
	public ResponseEntity<Void> addAppareil(@Valid @RequestBody Appareil appareil) {
		Appareil appareilAdded = appareilRepository.save(appareil);
		if (appareilAdded == null) {
			return ResponseEntity.noContent().build();
		}

		Logger.getLogger(this.getClass().getName()).info("Saved " + appareilAdded);
		Logger.getLogger(this.getClass().getName()).info("Appareils : " + getAppareils());

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(appareilAdded.getId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping("/appareils")
	public void modifyAppareils(@RequestBody List<Appareil> appareils) {
		appareilRepository.saveAll(appareils);
		Logger.getLogger(this.getClass().getName()).info("Saved " + appareils);
		Logger.getLogger(this.getClass().getName()).info("Appareils : " + getAppareils());
	}

	@ApiOperation(value = "Récupère un appareil grâce à son ID")
	@GetMapping(value = "/appareil/{id}")
	public Optional<Appareil> afficherUnProduit(@PathVariable long id) {
		Optional<Appareil> appareil = appareilRepository.findById(id);
		if (appareil.isEmpty())
			throw new RuntimeException("L'appareil avec l'id " + id + " est INTROUVABLE.");
		return appareil;
	}

	// Jpa findBy...
	@ApiOperation(value = "Récupère les appareils avec un nom qui ressemble au pattern donné")
	@GetMapping(value = "/AppareilByNameLike/{pattern}")
	// On peut du JQL
	// @Query("select a from Appareil a where a.name like %?1%")
	// ou du SQL
	@Query(value = "SELECT * FROM APPAREIL WHERE NAME LIKE %?1%", countQuery = "SELECT count(*) FROM APPAREIL WHERE NAME LIKE %?1%", nativeQuery = true)
	public List<Appareil> appareilsByNameLike(@PathVariable String pattern) {
		return appareilRepository.findByNameLike(pattern);
	}
}